#!/usr/bin/python

import subprocess
import sys

def main():
    vids = sys.argv[1:]
    
    for video in vids:
        subprocess.run(["mpv", video])

if __name__ == "__main__":
    main()
